# IoT

This repository contains the commands to communicate with the IoT PCB inside the SUCHAI 3 CubeSat. It uses the SUCHAI flight software.

### Working with the Raspberry Pi (RPI)

First download the following files and repositories:

```shell
git clone -b feature/framework https://gitlab.com/spel-uchile/suchai-flight-software
git clone https://gitlab.com/matvidalv/iot
```

Now, for the installation run the script init.sh:

```shell
cd iot/
sudo sh init.sh
```

Building for the Raspberry Pi is straight forward and very similar to any Linux.
Just connect to the RPi using SSH. Then, set proper build variables and build with
the following steps:

```shell
cd ../suchai-flight-software/
cmake -B build -DAPP=IoT -DSCH_ARCH=RPI -DSCH_ST_MODE=SQLITE -DSCH_COMM_NODE=7 && cmake --build build
./build/apps/IoT/IoT-app
```

### Supported hardware:

* Single board computer: Raspberry Pi Zero or 3B+

### Contact:

* Name: Matías Vidal Valladares
* e-mail: matias.vidal.v@gmail.com
