import serial
import sys
from time import time

args = sys.argv
app_name = args[1]
path = "apps/{}/src/system/".format(app_name)
timeout = args[2]

ser = serial.Serial("/dev/ttyUSB0", 115200)
if (not ser.is_open):
    ser.open()

start = int(time())
#timedelta = 10
data = []

while (ser.is_open):
    data.append(ser.readline().decode(errors="ignore").replace('\r', ''))
    current_time = int(time())
    if (current_time - start > int(timeout)):
        ser.close()

with open("{}/IoT_data.txt".format(path), 'w') as data_file:
    for line in data:
        data_file.write(line.replace('\x00', '').replace('\x01', '').replace('\x02', '').replace('\x03', '').replace('\x04', '').replace('\x05', '').replace('\x06', '').replace('\x07', '').replace('\x08', '').replace('\x09', '').replace('\x10', ''))

data_file.close()
sys.exit()
