/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2021, Carlos Gonzalez Cortes, carlgonz@uchile.cl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app/system/cmdIOT.h"

#define MAX_LINE_LENGTH 48

static const char* tag = "cmdIOT";
char iot_app_path[65];
char iot_find_cmd[101];

void cmd_iot_init(void) {
    cmd_add("iot_parse_data", iot_parse_data, "", 0);
    cmd_add("iot_receive_data", iot_receive_data, "%d", 1);
    cmd_add("iot_save_data", iot_save_data, "", 0);
    FILE *fp;
    snprintf(iot_find_cmd, sizeof(iot_find_cmd),
    "find /home -type d -name %s 2>/dev/null | grep -v build | grep -v super-mag-plus | tr -d '\n'", APP_NAME);
    fp = popen(iot_find_cmd, "r");
    fgets(iot_app_path, sizeof(iot_app_path), fp);
    pclose(fp);
}

int iot_parse_data(char *fmt, char *params, int nparams) {
    #ifdef LINUX
    char script_path[101];
    snprintf(script_path, sizeof(script_path), "python3 %s/src/system/IoT.py %s", iot_app_path, APP_NAME);
    LOGI(tag, "Running the command: %s", script_path);
    system(script_path);
    #endif
    return CMD_OK;
}

int iot_receive_data(char *fmt, char *params, int nparams) {
    uint32_t timeout = 10;
    if (params == NULL) {
	LOGE(tag, "NULL params!");
	return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &timeout) == nparams) {
        #ifdef LINUX
        char rx_cmd[115];
        snprintf(rx_cmd, sizeof(rx_cmd), "python3 %s/src/system/IoT_RX.py %s %u", iot_app_path, APP_NAME, timeout);
        LOGI(tag, "Running the command: %s", rx_cmd);
        system(rx_cmd);
	LOGI(tag, "Done");
        #endif
    }
    else {
	LOGE(tag, "Invalid params!");
	return CMD_SYNTAX_ERROR;
    }
    return CMD_OK;
}

int iot_save_data(char *fmt, char *params, int nparams) {
    iot_data_t iot_data; 
    char line[50] = {0};
    char data_path[95];
    int rc = 0;
    snprintf(data_path, sizeof(data_path), "%s/src/system/received_data.txt", iot_app_path);
    FILE *data_file = fopen(data_path, "r");
    if (data_file == NULL) {
        LOGE(tag, "Error. The file does not exist.\n");
        return CMD_ERROR;
    }
    while(fgets(line, 50, data_file)) {
        LOGI(tag, "%s", line);
        iot_data.index = dat_get_system_var(dat_drp_idx_iot);
        iot_data.timestamp = dat_get_time();
        sscanf(line, "%u %u %u %[^\n]", &iot_data.module,
				    &iot_data.temp1,
				    &iot_data.temp2,
				    iot_data.data);
	LOGI(tag, "%s", &iot_data.data)
	rc += dat_add_payload_sample(&iot_data, iot_sensor);
    }
    fclose(data_file);
    char buff[16];
    sprintf(buff, "rc=%d", rc);
    com_send_debug(10, buff, strlen(buff));
    return CMD_OK;
}
