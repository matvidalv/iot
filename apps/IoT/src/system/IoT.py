import sys

def parse_file(path):
    rtc = []
    received_commands = []
    modules = []
    temperatures = []
    with open(path + "IoT_data.txt") as IoT_data:
        lines = IoT_data.readlines()

    IoT_data.close()

    for i, l in enumerate(lines):
        if (l[:5] == "RTC: "):
            rtc.append(l[5:-1])
        if (l[:28] == "LAST_RECEIVE_REC_DATA_ASCII:"):
            if ('8' in l[29:]):
                received_commands.append(parse_line(l[29:]))
            else:
                received_commands.append(parse_line(lines[i+1]))
        if (l[:23] == "LAST_RECEIVE_MODULE_NO:"):
            modules.append(l[23:-1])
        if (l[:6] == "temp1:"):
            temperatures.append("{} {}".format(l[6:8], l[15:17]))

    with open(path + "received_data.txt", 'w') as data_file:
        for i, cmd in enumerate(received_commands):
            #print("{} {} {} {}".format(modules[i], temperatures[i], cmd, rtc[i]))
            data_file.write("{} {} {} {}\n".format(modules[i], temperatures[i], cmd, rtc[i]))

    data_file.close()

def parse_line(line):
    index = 0
    for index, character in enumerate(line):
        if (character == '8'):
            break
    return line[index+1:index + 7]

app_name = sys.argv
path = "apps/" + app_name[1] + "/src/system/"
parse_file(path)
