/**
 * @file  cmdIOT.h
 * @author Matías Vidal Valladares - matias.vidal@ing.uchile.cl
 * @date 2022
 * @copyright GNU GPL v3
 *
 * This header have definitions of commands related to the IoT payload.
 */

#ifndef CMD_IOT_H
#define CMD_IOT_H

#include "suchai/config.h"

#include "suchai/repoCommand.h"
#include "suchai/repoData.h"

/**
 * Register IoT commands
 */
void cmd_iot_init(void);

/**
 * It parses raw data and saves it in a text file.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_ERROR_SYNTAX in case of parameters errors
 */
int iot_parse_data(char *fmt, char *params, int nparams);

/**
 * Receives raw data from the IoT transceiver and saves it in a text file
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_ERROR_SYNTAX in case of parameters errors
 */
int iot_receive_data(char *fmt, char *params, int nparams);

/**
 * This function saves the processed data as payload data in the SUCHAI flight
 * software.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_ERROR_SYNTAX in case of parameters errors
 */
int iot_save_data(char *fmt, char *params, int nparams);

#endif /* CMD_IOT_H */
