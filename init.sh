#!/usr/bin/env bash
sudo apt-get install gcc make cmake pkg-config libzmq3-dev libsqlite3-dev libpq-dev python
cd ../suchai-flight-software/
sudo -u "$(logname)" cp -r ../iot/apps/IoT/ apps/
